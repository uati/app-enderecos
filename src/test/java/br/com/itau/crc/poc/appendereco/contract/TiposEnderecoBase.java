package br.com.itau.crc.poc.appendereco.contract;

import br.com.itau.crc.poc.appendereco.v1.api.EnderecoRestController;
import br.com.itau.crc.poc.appendereco.v1.api.TipoEnderecoRestController;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;

@RunWith(SpringRunner.class)
//@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class TiposEnderecoBase {

    @Autowired
    TipoEnderecoRestController tipoEndcontroller;

    @Before
    public void setup() {
        StandaloneMockMvcBuilder standaloneMockMvcBuilder = MockMvcBuilders.standaloneSetup(tipoEndcontroller);
        RestAssuredMockMvc.standaloneSetup(standaloneMockMvcBuilder);
    }
    @Test
    public void test_controllerNotNull(){
        Assert.assertNotNull(tipoEndcontroller);
    }


}
