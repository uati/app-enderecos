package contracts.enderecos

import org.springframework.cloud.contract.spec.Contract

//String stringArrayMayBeEmptyRegex = '(\\[(("[^"]*",)*("[^"]*"))*])'

Contract.make {
    request{
        method GET()
        url("/enderecos")
    }
    response {
        status 200
        body([
                numero: $(regex('[0-9]{1,2}|[0-9]{3}')),
                //numero: $(regex('^[0-9]{3}$')),
                cidade: null,
                id:  value($(anyNonBlankString())),
                //logradouro:  $(optional(anyNonBlankString())),
                logradouro: value($(anyNonBlankString())),
                bairro: value(consumer("Mooca"), producer(optional("Mooca"))),
        ])
    }
}