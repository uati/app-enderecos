package contracts.tipos

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request{
        method GET()
        url("/tipos/endereco")
    }
    response {
        status 200
        body([
                id:  value($(anyNonBlankString())),
                descricao: value($(anyNonBlankString()))
        ])
    }
}