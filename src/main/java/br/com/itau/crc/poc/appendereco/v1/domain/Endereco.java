package br.com.itau.crc.poc.appendereco.v1.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Endereco {
    private String id;
    private Integer numero;
    private String logradouro;
    private String cidade;
    private String bairro;
}
