package br.com.itau.crc.poc.appendereco.v1.dto;


import br.com.itau.crc.poc.appendereco.v1.domain.TipoEndereco;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoEnderecoDto {

    private String id;
    private String descricao;

    public TipoEnderecoDto(TipoEndereco tipoEndereco) {
        this.id = tipoEndereco.getId();
        this.descricao = tipoEndereco.getDescricao();
    }
}
