package br.com.itau.crc.poc.appendereco.v1.api;

import br.com.itau.crc.poc.appendereco.v1.domain.TipoEndereco;
import br.com.itau.crc.poc.appendereco.v1.dto.TipoEnderecoDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tipos/endereco")
public class TipoEnderecoRestController {

    @GetMapping
    public List<TipoEnderecoDto> getAll() {
        List<TipoEndereco> list = Arrays.asList(TipoEndereco.builder().id("34rerfdfg").descricao("COMERCIAL").build());
        List<TipoEnderecoDto> listDto = list.stream().map(TipoEnderecoDto::new).collect(Collectors.toList());
        return listDto;
    }

}
