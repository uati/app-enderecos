package br.com.itau.crc.poc.appendereco.v1.api;

import br.com.itau.crc.poc.appendereco.v1.domain.Endereco;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/enderecos")
public class EnderecoRestController {

    @GetMapping
    public List<Endereco> getAll() {
        return Arrays.asList(Endereco.builder().id("34rerfdfg").numero(1).logradouro("Av Paulista").build());
    }

}
