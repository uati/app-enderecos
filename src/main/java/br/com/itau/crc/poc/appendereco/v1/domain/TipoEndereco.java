package br.com.itau.crc.poc.appendereco.v1.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TipoEndereco {

    private String id;
    private String descricao;
}
